# Recipes for Openconnect VPN

This document contains How-to guides and recipes for various advanced
configuration settings with OpenConnect VPN server.

View at [docs.openconnect-vpn.net](https://docs.openconnect-vpn.net).

---
![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

## How to contribute

Documents are placed in [docs directory](docs/), and are written
in [python markdown](https://squidfunk.github.io/mkdocs-material/setup/extensions/python-markdown/?h=markdown)
with certain extensions such as emoji and admonitions. See
[mkdocs.yml](mkdocs.yml) for the enabled extensions.

The recommended path to contribute is to create new recipes and
submit as merge requests.

## How to build

The following will build the docs and create a local web
server to view.

```
$ pip install mkdocs mkdocs-material material-plausible-plugin
$ mkdocs serve
```
